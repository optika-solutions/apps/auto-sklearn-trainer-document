"""
Auto SK-Learn Model
"""

import autosklearn
import autosklearn.classification
import autosklearn.regression
import sklearn.model_selection
import sklearn.metrics
import sklearn.preprocessing
import pandas as pd
import requests
from datetime import datetime
from threading import Timer
from joblib import dump
from akumen_api import get_results, MODEL_NAME, AKUMEN_API_URL, API_KEY, progress


# disable sklearn warnings
import warnings
warnings.filterwarnings('ignore') 

TOTAL_TIME = 30


def akumen(source, **kwargs):
    """
    Parameters:
        - Input: source [scenario]
        - Input: scope [string]
        - Input: view_name [string]
        - Input: features [json]
        - Input: target [string]
        - Input: model_type [string]
        - Input: training_split [float:2]

        - Output: accuracy [float]
    """
    print('Running Akumen model...')
    
    if not kwargs.get('view_name'):
        raise Exception('A view_name must be specified to retrieve results data from a source model.')
    
    model_type = kwargs.get('model_type')
    if not model_type or model_type not in ['classification', 'regression']:
        raise Exception('model_type must be set to one of [\'classification\', \'regression\']')
    
    features = kwargs.get('features')
    if not features or ('numeric' not in features and 'categorical' not in features and 'datetime' not in features):
        raise Exception('Features must be specified in JSON format: {"numeric":[columns...],"categorical":[columns...],"datetime":[columns...]}')

    target = kwargs.get('target')
    if not target:
        raise Exception('You need to specify a target column in "target".')

    df = get_results(source, scope=kwargs.get('scope', 'scenario'), view_name=kwargs.get('view_name'))
    
    # remove akumen standard columns
    df.drop(list(df.filter(regex = 'NamelessColumn_')), axis=1, inplace=True)
    df.drop(['scenarioname', 'studyname', 'id'], axis=1, inplace=True)
    
    # convert as required
    all_features = []
    feat_types = []
    for dtype in features.keys():
        all_features.extend(features[dtype])
        for key in features[dtype]:
            if dtype == 'numeric':
                df[key] = pd.to_numeric(df[key])
                feat_types.append('Numerical')
            elif dtype == 'datetime':
                df[key] = pd.to_datetime(df[key])
                df[key] = df[key].apply(datetime.toordinal)
                feat_types.append('Numerical')
            elif dtype == 'categorical':
                df[key] = df[key].astype('category').cat.codes
                feat_types.append('Categorical')
            else:
                raise Exception('Data type specified is not supported.')
    
    le = None
    if model_type == 'classification':
        le = sklearn.preprocessing.LabelEncoder()
        le.fit(df[target])
        df[target] = le.transform(df[target])
    
    x_train, x_test, y_train, y_test = sklearn.model_selection.train_test_split(df[all_features], df[target], train_size=kwargs.get('training_split', 0.7))
    if model_type == 'classification':
        automl = autosklearn.classification.AutoSklearnClassifier(
            time_left_for_this_task=TOTAL_TIME,
            per_run_time_limit=5,
            memory_limit=2048
        )
    elif model_type == 'regression':
        automl = autosklearn.regression.AutoSklearnRegressor(
            time_left_for_this_task=TOTAL_TIME,
            per_run_time_limit=5,
            memory_limit=2048
        )
    
    automl.fit(x_train, y_train)
    y_hat = automl.predict(x_test)
    if model_type == 'classification':
        accuracy = sklearn.metrics.accuracy_score(y_test, y_hat)
    elif model_type == 'regression':
        accuracy = sklearn.metrics.explained_variance_score(y_test, y_hat)
    automl.akumen_features = features
    if le:
        automl.akumen_labeller = le
    
    print(automl.cv_results_)
    automl.sprint_statistics()
    
    model_file = f'{MODEL_NAME}.joblib'
    dump(automl, 'saved')
    headers = { 'authorization': API_KEY }
    url = AKUMEN_API_URL + 'documents/' + 'New/' + 'create' + '?overwrite_existing=true'

    f = open('saved', "rb")
    reader = f.read()
    f.close()

    files = {'file': (model_file.replace('\n', '').replace('\r', '') + '.csv', reader, 'application/x-binary')}
    response = requests.post(url, files=files, headers=headers)

    return {
        'accuracy': accuracy * 100
    }
